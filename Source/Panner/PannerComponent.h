/*
  ==============================================================================

    PannerComponent.h
    Created: 24 Jan 2021 4:40:22pm
    Author:  Robin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

class NaiteWhistleAudioProcessor;
//==============================================================================
/*
*/
class PannerComponent  : public juce::Component
{
public:
    PannerComponent(NaiteWhistleAudioProcessor& p);
    ~PannerComponent() override;

    void paint (juce::Graphics&) override;
    void resized() override;
    inline static char* ID{"PANNER"};
	inline static char* name{"Panner"};
    inline static float minValue{-1.0f};
	inline static float maxValue{1.0f};
	inline static float defaultValue{0.0f};
private:
	NaiteWhistleAudioProcessor& audioProcessor;

	std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> pannerAttachment;
    juce::Slider pannerSlider;
    juce::Label pannerLabel;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (PannerComponent)
};
