/*
  ==============================================================================

    PannerComponent.cpp
    Created: 24 Jan 2021 4:40:22pm
    Author:  Robin

  ==============================================================================
*/

#include <JuceHeader.h>
#include "PannerComponent.h"
#include "../PluginProcessor.h"

//==============================================================================
PannerComponent::PannerComponent(NaiteWhistleAudioProcessor& p) : audioProcessor(p)
{
    const float labelFontSize{15.0f};
	const float interval{0.01f};
	const int textBoxWidth{40};
	const int textBoxHeight{20};
	

	pannerSlider.setSliderStyle(juce::Slider::LinearBar);
	pannerSlider.setTextBoxStyle(juce::Slider::TextBoxBelow,true,textBoxWidth,textBoxHeight);
	pannerSlider.setRange(minValue,maxValue,interval);
	addAndMakeVisible(pannerSlider);
	pannerLabel.setFont(labelFontSize);
	pannerLabel.setText(name,juce::dontSendNotification);
	pannerLabel.setJustificationType(juce::Justification::centredBottom);
	pannerLabel.attachToComponent(&pannerSlider,false);

	pannerAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment>(audioProcessor.GetAPVT(),ID,pannerSlider);

}

PannerComponent::~PannerComponent()
{
}

void PannerComponent::paint (juce::Graphics& g)
{
	g.fillAll(juce::Colours::green);
}

void PannerComponent::resized()
{
	pannerSlider.setBoundsRelative(0.05f,0.30f,0.9f,0.5f);
}
