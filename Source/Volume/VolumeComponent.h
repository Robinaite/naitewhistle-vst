/*
  ==============================================================================

    VolumeComponent.h
    Created: 24 Jan 2021 2:55:43pm
    Author:  Robin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

class NaiteWhistleAudioProcessor;
//==============================================================================
/*
*/
class VolumeComponent  : public juce::Component
{
public:
    VolumeComponent(NaiteWhistleAudioProcessor& p);
    ~VolumeComponent() override;

    void paint (juce::Graphics&) override;
    void resized() override;
    inline static char* ID{"VOLUME"};
	inline static char* name{"Volume"};
    inline static float minValue{0.0f};
	inline static float maxValue{1.0f};
	inline static float defaultValue{0.70f};
private:
	NaiteWhistleAudioProcessor& audioProcessor;

	std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> volumeAttachment;
    juce::Slider volumeSlider;
    juce::Label volumeLabel;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (VolumeComponent)
};
