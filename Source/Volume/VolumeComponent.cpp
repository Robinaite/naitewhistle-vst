/*
  ==============================================================================

    VolumeComponent.cpp
    Created: 24 Jan 2021 2:55:43pm
    Author:  Robin

  ==============================================================================
*/

#include <JuceHeader.h>
#include "VolumeComponent.h"
#include "../PluginProcessor.h"

//==============================================================================
VolumeComponent::VolumeComponent(NaiteWhistleAudioProcessor& p) : audioProcessor(p) 
{
    const float labelFontSize{15.0f};
	const float interval{0.01f};
	const int textBoxWidth{40};
	const int textBoxHeight{20};
	

	volumeSlider.setSliderStyle(juce::Slider::RotaryVerticalDrag);
	volumeSlider.setTextBoxStyle(juce::Slider::TextBoxBelow,true,textBoxWidth,textBoxHeight);
	volumeSlider.setRange(minValue,maxValue,interval);
	addAndMakeVisible(volumeSlider);
	volumeLabel.setFont(labelFontSize);
	volumeLabel.setText(name,juce::dontSendNotification);
	volumeLabel.setJustificationType(juce::Justification::centredBottom);
	volumeLabel.attachToComponent(&volumeSlider,false);

	volumeAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment>(audioProcessor.GetAPVT(),ID,volumeSlider);

}

VolumeComponent::~VolumeComponent()
{
}

void VolumeComponent::paint (juce::Graphics& g)
{
	g.fillAll(juce::Colours::black);
}

void VolumeComponent::resized()
{
	volumeSlider.setBoundsRelative(0,0.16f,1,0.75f);
}
