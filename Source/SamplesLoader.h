#pragma once
#include "juce_audio_formats/juce_audio_formats.h"


class NaiteSynth;

enum class Articulation
{
	Sustain,
	Staccato
};

class SampleLoader
{
public:
	SampleLoader();
	

	void LoadSamples(NaiteSynth& sampler, Articulation articulation);
private:
	void CreateAndAddSound(NaiteSynth& sampler,juce::File &sampleFile,const int startMidiKey,const int numMidiKeys, const int milisecondsLoop);
	
	juce::AudioFormatManager formatManager;
	std::map<Articulation,juce::String> articulationFolder{{Articulation::Sustain,"sustain"},{Articulation::Staccato,"staccato"}};
};
