/*
  ==============================================================================

    LoopSamplerSound.cpp
    Created: 24 Jan 2021 10:13:40pm
    Author:  Robin

  ==============================================================================
*/

#include "LoopSamplerSound.h"
#include "juce_audio_formats/juce_audio_formats.h"

LoopSamplerSound::LoopSamplerSound (const juce::String& soundName,
                                    juce::AudioFormatReader& source,
                            const juce::BigInteger& notes,
                            int midiNoteForNormalPitch,
                            double attackTimeSecs,
                            double releaseTimeSecs,
                            double maxSampleLengthSeconds,const int millisecondsLoopTime)
    : name (soundName),
      sourceSampleRate (source.sampleRate),
      midiNotes (notes),
      midiRootNote (midiNoteForNormalPitch), secondsLoopTime(static_cast<float>(millisecondsLoopTime)/1000.0f)
{
    if (sourceSampleRate > 0 && source.lengthInSamples > 0)
    {
        length = juce::jmin ((int) source.lengthInSamples,
                             (int) (maxSampleLengthSeconds * sourceSampleRate));

        data.reset (new juce::AudioBuffer<float> (juce::jmin (2, (int) source.numChannels), length + 4));

        source.read (data.get(), 0, length + 4, 0, true, true);

        params.attack  = static_cast<float> (attackTimeSecs);
        params.release = static_cast<float> (releaseTimeSecs);
    }
}

LoopSamplerSound::~LoopSamplerSound()
{
}

bool LoopSamplerSound::appliesToNote (int midiNoteNumber)
{
    return midiNotes[midiNoteNumber];
}

bool LoopSamplerSound::appliesToChannel (int /*midiChannel*/)
{
    return true;
}