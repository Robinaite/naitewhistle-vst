/*
  ==============================================================================

    LoopSamplerVoice.h
    Created: 24 Jan 2021 9:21:13pm
    Author:  Robin

  ==============================================================================
*/

#pragma once
#include "juce_audio_basics/juce_audio_basics.h"

class LoopSamplerVoice : public juce::SynthesiserVoice
{
public:
    //==============================================================================
    /** Creates a SamplerVoice. */
    LoopSamplerVoice();

    /** Destructor. */
    ~LoopSamplerVoice() override;

    //==============================================================================
    bool canPlaySound (juce::SynthesiserSound*) override;

    void startNote (int midiNoteNumber, float velocity, juce::SynthesiserSound*, int pitchWheel) override;
    void stopNote (float velocity, bool allowTailOff) override;

    void pitchWheelMoved (int newValue) override;
    void controllerMoved (int controllerNumber, int newValue) override;

    void renderNextBlock (juce::AudioBuffer<float>&, int startSample, int numSamples) override;
    using SynthesiserVoice::renderNextBlock;

private:
	const float FadeStartSecondsFromEnd{0.2f};
   
    double pitchRatio = 0;
    double sourceSamplePosition = 0;
    float lgain = 0, rgain = 0;

    juce::ADSR adsr;

	 //==============================================================================
    JUCE_LEAK_DETECTOR (LoopSamplerVoice)
};
