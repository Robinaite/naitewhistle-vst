/*
  ==============================================================================

    LoopSamplerVoice.cpp
    Created: 24 Jan 2021 9:21:13pm
    Author:  Robin

  ==============================================================================
*/

#include "LoopSamplerVoice.h"
#include "LoopSamplerSound.h"

LoopSamplerVoice::LoopSamplerVoice() {}
LoopSamplerVoice::~LoopSamplerVoice() {}

bool LoopSamplerVoice::canPlaySound (juce::SynthesiserSound* sound)
{
    return dynamic_cast<const LoopSamplerSound*> (sound) != nullptr;
}

void LoopSamplerVoice::startNote (int midiNoteNumber, float velocity, juce::SynthesiserSound* s, int /*currentPitchWheelPosition*/)
{
    if (const auto* sound = dynamic_cast<const LoopSamplerSound*> (s))
    {
        pitchRatio = std::pow (2.0, (static_cast<double>(midiNoteNumber) - sound->midiRootNote) / 12.0)
                        * sound->sourceSampleRate / getSampleRate();

        sourceSamplePosition = 0.0;
        lgain = velocity;
        rgain = velocity;

        adsr.setSampleRate (sound->sourceSampleRate);
        adsr.setParameters (sound->params);

        adsr.noteOn();
    }
    else
    {
        jassertfalse; // this object can only play LoopSamplerSounds!
    }
}

void LoopSamplerVoice::stopNote (float /*velocity*/, bool allowTailOff)
{
    if (allowTailOff)
    {
        adsr.noteOff();
    }
    else
    {
        clearCurrentNote();
        adsr.reset();
    }
}

void LoopSamplerVoice::pitchWheelMoved (int /*newValue*/) {}
void LoopSamplerVoice::controllerMoved (int /*controllerNumber*/, int /*newValue*/) {}

//==============================================================================
void LoopSamplerVoice::renderNextBlock (juce::AudioBuffer<float>& outputBuffer, int startSample, int numSamples)
{
    if (auto* playingSound = dynamic_cast<LoopSamplerSound*> (getCurrentlyPlayingSound().get()))
    {
        auto& data = *playingSound->data;
        const float* const inL = data.getReadPointer (0);
        const float* const inR = data.getNumChannels() > 1 ? data.getReadPointer (1) : nullptr;

        float* outL = outputBuffer.getWritePointer (0, startSample);
        float* outR = outputBuffer.getNumChannels() > 1 ? outputBuffer.getWritePointer (1, startSample) : nullptr;

        const auto soundLength = playingSound->length;
    	const auto fadeStartValue = soundLength - getSampleRate() * playingSound->secondsLoopTime; 
    	
        while (--numSamples >= 0)
        {
            const auto pos = static_cast<int>(sourceSamplePosition);
            const auto alpha = static_cast<float>(sourceSamplePosition - pos);
            const auto invAlpha = 1.0f - alpha;

        	const auto fadePosition = static_cast<int>(sourceSamplePosition-fadeStartValue);
        	
        	auto inLVal = inL[pos];
        	auto inLValPlusOne = inL[pos+1];
        	auto inRVal = inR != nullptr ? inR[pos] : inLVal;
        	auto inRValPlusOne = inR != nullptr ? inR[pos+1] : inLValPlusOne;
        	if(sourceSamplePosition > fadeStartValue) //Fade-out last part and fade-in loop
        	{
        		const auto volumeFadeVal = juce::jmap<float>(fadePosition,0.0f,soundLength - fadeStartValue,0.0f,1.0f);
        		inLVal = inLVal * (1.0f-volumeFadeVal) + (inL[fadePosition] * volumeFadeVal);
        		inLValPlusOne = inLValPlusOne * (1.0f-volumeFadeVal) + (inL[fadePosition+1] * volumeFadeVal);

        		if(inR)
        		{
        			inRVal = inRVal * (1.0f-volumeFadeVal) + (inR[fadePosition] * volumeFadeVal);
        			inRValPlusOne = inRValPlusOne * (1.0f-volumeFadeVal) + (inR[fadePosition+1] * volumeFadeVal);
        		} else
        		{
        			inRVal = inLVal;
        			inRValPlusOne = inLValPlusOne;
        		}
        	}

            // just using a very simple linear interpolation here..
            float l = (inLVal * invAlpha + inLValPlusOne * alpha);
            float r = (inR != nullptr) ? (inRVal * invAlpha + inRValPlusOne * alpha) : l;

            const auto envelopeValue = adsr.getNextSample();

            l *= lgain * envelopeValue;
            r *= rgain * envelopeValue;

            if (outR != nullptr)
            {
                *outL++ += l;
                *outR++ += r;
            }
            else
            {
                *outL++ += (l + r) * 0.5f;
            }

            sourceSamplePosition += pitchRatio;

            if (sourceSamplePosition > soundLength) 
            {
            	sourceSamplePosition = soundLength - fadeStartValue;
            }
        }
    }
}
