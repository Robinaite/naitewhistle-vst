#pragma once


struct ReverbValues
{
	ReverbValues() = delete;
	ReverbValues(const char* idString, const char* nameString, const float minValue, const float maxValue, const float defaultValue) : ID(idString), name(nameString), minValue(minValue),
		maxValue(maxValue), defaultValue(defaultValue)
	{}
	const char* ID;
	const char* name;
	const float minValue;
	const float maxValue;
	const float defaultValue;
};

inline const ReverbValues roomSizeValue{"ROOM_SIZE","Room Size",0.0f,1.0f,0.2f};
inline const ReverbValues dampingValue{"DAMPING","Damping",0.0f,1.0f,1.0f};
inline const ReverbValues wetLevelValue{"WET_LEVEL","Wet Level",0.0f,1.0f,0.20f};
inline const ReverbValues dryLevelValue{"DRY_LEVEL","Dry Level",0.0f,1.0f,0.60f};
inline const ReverbValues widthValue{"WIDTH","Width",0.0f,1.0f,1.0f};
inline const ReverbValues freezeModeValue{"FREEZE_MODE","Freeze Mode",0.0f,1.0f,0.0f};