/*
  ==============================================================================

    ADSRComponent.cpp
    Created: 23 Jan 2021 7:20:26pm
    Author:  Robin

  ==============================================================================
*/

#include "ReverbComponent.h"
#include "../PluginProcessor.h"
#include "ReverbVal.h"

//==============================================================================
ReverbComponent::ReverbComponent(NaiteWhistleAudioProcessor& p) : audioProcessor(p)
{
	const float labelFontSize{15.0f};
	const float interval{0.01f};
	const int textBoxWidth{40};
	const int textBoxHeight{20};

	reverbTitle.setFont(labelFontSize);
	reverbTitle.setText("Reverb",juce::dontSendNotification);
	reverbTitle.setJustificationType(juce::Justification::centredTop);
	addAndMakeVisible(reverbTitle);
	
	
    roomSizeSlider.setSliderStyle(juce::Slider::RotaryVerticalDrag);
	roomSizeSlider.setTextBoxStyle(juce::Slider::TextBoxBelow,true,textBoxWidth,textBoxHeight);
	roomSizeSlider.setRange(roomSizeValue.minValue,roomSizeValue.maxValue,interval);
	addAndMakeVisible(roomSizeSlider);
	roomSizeLabel.setFont(labelFontSize);
	roomSizeLabel.setText(roomSizeValue.name,juce::dontSendNotification);
	roomSizeLabel.setJustificationType(juce::Justification::centredBottom);
	roomSizeLabel.attachToComponent(&roomSizeSlider,false);

	roomSizeAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment>(audioProcessor.GetAPVT(),roomSizeValue.ID,roomSizeSlider);
	
	
	dampingSlider.setSliderStyle(juce::Slider::RotaryVerticalDrag);
	dampingSlider.setTextBoxStyle(juce::Slider::TextBoxBelow,true,textBoxWidth,textBoxHeight);
	dampingSlider.setRange(dampingValue.minValue,dampingValue.maxValue,interval);
	addAndMakeVisible(dampingSlider);

	dampingLabel.setFont(labelFontSize);
	dampingLabel.setText(dampingValue.name,juce::dontSendNotification);
	dampingLabel.setJustificationType(juce::Justification::centredBottom);
	dampingLabel.attachToComponent(&dampingSlider,false);

	dampingAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment>(audioProcessor.GetAPVT(),dampingValue.ID,dampingSlider);
	
	wetLevelSlider.setSliderStyle(juce::Slider::RotaryVerticalDrag);
	wetLevelSlider.setTextBoxStyle(juce::Slider::TextBoxBelow,true,textBoxWidth,textBoxHeight);
	wetLevelSlider.setRange(wetLevelValue.minValue,wetLevelValue.maxValue,interval);
	addAndMakeVisible(wetLevelSlider);

	wetLevelLabel.setFont(labelFontSize);
	wetLevelLabel.setText(wetLevelValue.name,juce::dontSendNotification);
	wetLevelLabel.setJustificationType(juce::Justification::centredBottom);
	wetLevelLabel.attachToComponent(&wetLevelSlider,false);

	wetLevelAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment>(audioProcessor.GetAPVT(),wetLevelValue.ID,wetLevelSlider);

	dryLevelSlider.setSliderStyle(juce::Slider::RotaryVerticalDrag);
	dryLevelSlider.setTextBoxStyle(juce::Slider::TextBoxBelow,true,textBoxWidth,textBoxHeight);
	dryLevelSlider.setRange(dryLevelValue.minValue,dryLevelValue.maxValue,interval);
	addAndMakeVisible(dryLevelSlider);

	dryLevelLabel.setFont(labelFontSize);
	dryLevelLabel.setText(dryLevelValue.name,juce::dontSendNotification);
	dryLevelLabel.setJustificationType(juce::Justification::centredBottom);
	dryLevelLabel.attachToComponent(&dryLevelSlider,false);

	dryLevelAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment>(audioProcessor.GetAPVT(),dryLevelValue.ID,dryLevelSlider);

	widthSlider.setSliderStyle(juce::Slider::RotaryVerticalDrag);
	widthSlider.setTextBoxStyle(juce::Slider::TextBoxBelow,true,textBoxWidth,textBoxHeight);
	widthSlider.setRange(widthValue.minValue,widthValue.maxValue,interval);
	addAndMakeVisible(widthSlider);

	widthLabel.setFont(labelFontSize);
	widthLabel.setText(widthValue.name,juce::dontSendNotification);
	widthLabel.setJustificationType(juce::Justification::centredBottom);
	widthLabel.attachToComponent(&widthSlider,false);

	widthAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment>(audioProcessor.GetAPVT(),widthValue.ID,widthSlider);
	
	freezeModeSlider.setSliderStyle(juce::Slider::RotaryVerticalDrag);
	freezeModeSlider.setTextBoxStyle(juce::Slider::TextBoxBelow,true,textBoxWidth,textBoxHeight);
	freezeModeSlider.setRange(freezeModeValue.minValue,freezeModeValue.maxValue,interval);
	addAndMakeVisible(freezeModeSlider);

	freezeModeLabel.setFont(labelFontSize);
	freezeModeLabel.setText(freezeModeValue.name,juce::dontSendNotification);
	freezeModeLabel.setJustificationType(juce::Justification::centredBottom);
	freezeModeLabel.attachToComponent(&freezeModeSlider,false);

	freezeModeAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment>(audioProcessor.GetAPVT(),freezeModeValue.ID,freezeModeSlider);
}

ReverbComponent::~ReverbComponent()
{
}

void ReverbComponent::paint (juce::Graphics& g)
{
	g.fillAll(juce::Colours::blueviolet);
}

void ReverbComponent::resized()
{
	const auto startX = 0.0f;
	const auto startY = 0.15f;
	const auto sliderWidth = 1.0f/3.0f;
	const auto sliderHeight = 0.35f;

	reverbTitle.setBoundsRelative(startX,0.0f,1.0f,0.1f);
	
	roomSizeSlider.setBoundsRelative(startX,startY,sliderWidth,sliderHeight);
	dampingSlider.setBoundsRelative(startX+sliderWidth,startY,sliderWidth,sliderHeight);
	wetLevelSlider.setBoundsRelative(startX+2*sliderWidth,startY,sliderWidth,sliderHeight);
	dryLevelSlider.setBoundsRelative(startX,1.7*startY + sliderHeight,sliderWidth,sliderHeight);
	widthSlider.setBoundsRelative(startX+sliderWidth,1.7*startY + sliderHeight,sliderWidth,sliderHeight);
	freezeModeSlider.setBoundsRelative(startX+2*sliderWidth,1.7*startY + sliderHeight,sliderWidth,sliderHeight);
}
