/*
  ==============================================================================

    ADSRComponent.h
    Created: 23 Jan 2021 7:20:26pm
    Author:  Robin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

class NaiteWhistleAudioProcessor;
//==============================================================================
/*
*/
class ReverbComponent  : public juce::Component
{
public:
    ReverbComponent(NaiteWhistleAudioProcessor& p);
    ~ReverbComponent() override;

    void paint (juce::Graphics&) override;
    void resized() override;

private:
    NaiteWhistleAudioProcessor& audioProcessor;
	
	std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> roomSizeAttachment,dampingAttachment,wetLevelAttachment,dryLevelAttachment,widthAttachment,freezeModeAttachment;

    juce::Slider roomSizeSlider, dampingSlider,wetLevelSlider,dryLevelSlider,widthSlider,freezeModeSlider;
    juce::Label roomSizeLabel, dampingLabel, wetLevelLabel, dryLevelLabel,widthLabel,freezeModeLabel,reverbTitle;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ReverbComponent)
};

