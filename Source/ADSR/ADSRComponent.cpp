/*
  ==============================================================================

    ADSRComponent.cpp
    Created: 23 Jan 2021 7:20:26pm
    Author:  Robin

  ==============================================================================
*/

#include "ADSRComponent.h"
#include "../PluginProcessor.h"
#include "ADSRVal.h"

//==============================================================================
ADSRComponent::ADSRComponent(NaiteWhistleAudioProcessor& p) : audioProcessor(p)
{
	const float labelFontSize{15.0f};
	const float interval{0.01f};
	const int textBoxWidth{40};
	const int textBoxHeight{20};
	
    attackSlider.setSliderStyle(juce::Slider::RotaryVerticalDrag);
	attackSlider.setTextBoxStyle(juce::Slider::TextBoxBelow,true,textBoxWidth,textBoxHeight);
	attackSlider.setRange(attackValue.minValue,attackValue.maxValue,interval);
	addAndMakeVisible(attackSlider);
	attackLabel.setFont(labelFontSize);
	attackLabel.setText(attackValue.name,juce::dontSendNotification);
	attackLabel.setJustificationType(juce::Justification::centredBottom);
	attackLabel.attachToComponent(&attackSlider,false);

	attackAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment>(audioProcessor.GetAPVT(),attackValue.ID,attackSlider);
	
	
	decaySlider.setSliderStyle(juce::Slider::RotaryVerticalDrag);
	decaySlider.setTextBoxStyle(juce::Slider::TextBoxBelow,true,textBoxWidth,textBoxHeight);
	decaySlider.setRange(decayValue.minValue,decayValue.maxValue,interval);
	addAndMakeVisible(decaySlider);

	decayLabel.setFont(labelFontSize);
	decayLabel.setText(decayValue.name,juce::dontSendNotification);
	decayLabel.setJustificationType(juce::Justification::centredBottom);
	decayLabel.attachToComponent(&decaySlider,false);

	decayAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment>(audioProcessor.GetAPVT(),decayValue.ID,decaySlider);
	
	sustainSlider.setSliderStyle(juce::Slider::RotaryVerticalDrag);
	sustainSlider.setTextBoxStyle(juce::Slider::TextBoxBelow,true,textBoxWidth,textBoxHeight);
	sustainSlider.setRange(sustainValue.minValue,sustainValue.maxValue,interval);
	addAndMakeVisible(sustainSlider);

	sustainLabel.setFont(labelFontSize);
	sustainLabel.setText(sustainValue.name,juce::dontSendNotification);
	sustainLabel.setJustificationType(juce::Justification::centredBottom);
	sustainLabel.attachToComponent(&sustainSlider,false);

	sustainAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment>(audioProcessor.GetAPVT(),sustainValue.ID,sustainSlider);
	
	releaseSlider.setSliderStyle(juce::Slider::RotaryVerticalDrag);
	releaseSlider.setTextBoxStyle(juce::Slider::TextBoxBelow,true,textBoxWidth,textBoxHeight);
	releaseSlider.setRange(releaseValue.minValue,releaseValue.maxValue,interval);
	addAndMakeVisible(releaseSlider);

	releaseLabel.setFont(labelFontSize);
	releaseLabel.setText(releaseValue.name,juce::dontSendNotification);
	releaseLabel.setJustificationType(juce::Justification::centredBottom);
	releaseLabel.attachToComponent(&releaseSlider,false);

	releaseAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment>(audioProcessor.GetAPVT(),releaseValue.ID,releaseSlider);
}

ADSRComponent::~ADSRComponent()
{
}

void ADSRComponent::paint (juce::Graphics& g)
{
    g.fillAll (juce::Colours::brown);   // clear the background
}

void ADSRComponent::resized()
{
	const auto startX = 0.0f;
	const auto startY = 0.2f;
	const auto sliderWidth = 1.0f/4.0f;
	const auto sliderHeight = 0.75f;
	
	attackSlider.setBoundsRelative(startX,startY,sliderWidth,sliderHeight);
	decaySlider.setBoundsRelative(startX+sliderWidth,startY,sliderWidth,sliderHeight);
	sustainSlider.setBoundsRelative(startX+2*sliderWidth,startY,sliderWidth,sliderHeight);
	releaseSlider.setBoundsRelative(startX+3*sliderWidth,startY,sliderWidth,sliderHeight);
}
