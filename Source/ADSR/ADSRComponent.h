/*
  ==============================================================================

    ADSRComponent.h
    Created: 23 Jan 2021 7:20:26pm
    Author:  Robin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

class NaiteWhistleAudioProcessor;
//==============================================================================
/*
*/
class ADSRComponent  : public juce::Component
{
public:
    ADSRComponent(NaiteWhistleAudioProcessor& p);
    ~ADSRComponent() override;

    void paint (juce::Graphics&) override;
    void resized() override;

private:
    NaiteWhistleAudioProcessor& audioProcessor;
	
	std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> attackAttachment,decayAttachment,sustainAttachment,releaseAttachment;

    juce::Slider attackSlider, decaySlider,sustainSlider,releaseSlider;
    juce::Label attackLabel, decayLabel, sustainLabel, releaseLabel;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ADSRComponent)
};

