#pragma once


struct ADSRValues
{
	ADSRValues() = delete;
	ADSRValues(const char* idString, const char* nameString, const float minValue, const float maxValue, const float defaultValue) : ID(idString), name(nameString), minValue(minValue),
		maxValue(maxValue), defaultValue(defaultValue)
	{}
	const char* ID;
	const char* name;
	const float minValue;
	const float maxValue;
	const float defaultValue;
};

inline const ADSRValues attackValue{"ATTACK","Attack",0.0f,5.0f,0.0f};
inline const ADSRValues decayValue{"DECAY","Decay",0.0f,3.0f,2.0f};
inline const ADSRValues sustainValue{"SUSTAIN","Sustain",0.0f,1.0f,1.0f};
inline const ADSRValues releaseValue{"RELEASE","Release",0.0f,5.0f,0.5f};