/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
NaiteWhistleAudioProcessorEditor::NaiteWhistleAudioProcessorEditor (NaiteWhistleAudioProcessor& p)
    : AudioProcessorEditor (&p), audioProcessor (p),adsrComp(p), volumeComponent(p), pannerComponent(p), reverbComponent(p)
{


    addAndMakeVisible(adsrComp);
	addAndMakeVisible(volumeComponent);
	addAndMakeVisible(pannerComponent);
	addAndMakeVisible(reverbComponent);
	
    setSize (400, 200);
}

NaiteWhistleAudioProcessorEditor::~NaiteWhistleAudioProcessorEditor()
{
}

//==============================================================================
void NaiteWhistleAudioProcessorEditor::paint (juce::Graphics& g)
{

}

void NaiteWhistleAudioProcessorEditor::resized()
{

	volumeComponent.setBoundsRelative(0.85f,0.0f,0.15f,0.5f);
	pannerComponent.setBoundsRelative(0.5f,0.0f,0.35f,0.5f);
	adsrComp.setBoundsRelative(0.5f,0.5f,0.5f,0.5f);
	reverbComponent.setBoundsRelative(0.0f,0.0f,0.5f,1);
}
