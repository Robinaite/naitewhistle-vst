/*
  ==============================================================================

    NaiteSynth.h
    Created: 26 Jan 2021 3:35:08pm
    Author:  Robin

  ==============================================================================
*/

#pragma once
#include "juce_audio_basics/juce_audio_basics.h"

class NaiteSynth : public juce::Synthesiser
{
protected:
	virtual juce::SynthesiserVoice* findVoiceToSteal(juce::SynthesiserSound* soundToPlay, int midiChannel, int midiNoteNumber) const override;
};

