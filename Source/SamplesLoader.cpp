#include "SamplesLoader.h"
#include "LoopSampler/LoopSamplerSound.h"
#include "Synthesizer/NaiteSynth.h"

SampleLoader::SampleLoader()
{
	formatManager.registerBasicFormats();
}

void SampleLoader::CreateAndAddSound(NaiteSynth& sampler,juce::File &sampleFile,const int startMidiKey,const int numMidiKeys, const int millisecondsLoop)
{
	std::unique_ptr<juce::AudioFormatReader> reader{ formatManager.createReaderFor(sampleFile) };
	juce::BigInteger keyboardRange{};
	keyboardRange.setRange(startMidiKey, numMidiKeys, true);
	sampler.addSound(new LoopSamplerSound(sampleFile.getFileNameWithoutExtension(), *reader, keyboardRange, startMidiKey, 0.1, 0.1, 14,millisecondsLoop));
}

void SampleLoader::LoadSamples(NaiteSynth& sampler, Articulation articulation)
{
	//File names are currently just the midi key note number, should be in folder "samples" next to the vst file.
	const juce::File samplesDirectory{ juce::File::getSpecialLocation(juce::File::currentApplicationFile).getParentDirectory().getChildFile("NaiteTinWhistleSamples") };
	if(!samplesDirectory.exists())
	{
		//Unable to load samples, can't find samples folder
		return;
	}
	const auto articulationDirectory{samplesDirectory.getChildFile(articulationFolder[articulation])};
	if(!articulationDirectory.exists())
	{
		//unable to load samples, can't find articulation folder
		return;
	}
	
	if (articulationDirectory.isDirectory())
	{
		juce::Array<juce::File> samples = articulationDirectory.findChildFiles(juce::File::TypesOfFileToFind::findFiles, false, "*.wav");

		if(samples.size() <= 0)
		{
			//Cant find any .wav files in the folder.
			return;
		}
		
		std::map<int, juce::File*> midiToFile;
		std::map<int,int> midiToMilisecondsLoop;

		for (juce::File& sampleFile : samples)
		{
			//FIleNameFormat XXX-YYY, where X is the amount of miliseconds the sample should start loop, Y the respective midi key note.
			auto fileName = sampleFile.getFileNameWithoutExtension();
			const auto indexOfSep = fileName.indexOf("-");
			if(indexOfSep == -1 || indexOfSep < 1)
			{
				//skip file as it has wrong naming conventions
				continue;
			}
			
			const auto miliSecondsString = fileName.substring(0,indexOfSep);
			const auto miliSeconds = miliSecondsString.getTrailingIntValue();
			if(!miliSeconds)
			{
				//skip file as it has wrong naming conventions
				continue;
			}

			auto midiKeyNoteVal = sampleFile.getFileNameWithoutExtension().getTrailingIntValue();
			if(!midiKeyNoteVal)
			{
				//skip file as it has wrong naming conventions
				continue;
			}
			
			if(midiKeyNoteVal < 0)
			{
				midiKeyNoteVal *= -1;
			}
			midiToFile[midiKeyNoteVal] = &sampleFile;
			midiToMilisecondsLoop[midiKeyNoteVal] = miliSeconds;
		}

		auto it = midiToFile.begin();
		while (it != midiToFile.end())
		{
			const auto &[midiKeyFirst, sampleFileFirst] = *it;
			it++;
			if (it == midiToFile.end())
			{
				CreateAndAddSound(sampler,*sampleFileFirst,midiKeyFirst,1,midiToMilisecondsLoop[midiKeyFirst]);
				break;
			}
			const auto &[midiKeySecond, sampleFileSecond] = *it;
			CreateAndAddSound(sampler,*sampleFileFirst,midiKeyFirst,midiKeySecond-midiKeyFirst,midiToMilisecondsLoop[midiKeyFirst]);
		}
	}
}
