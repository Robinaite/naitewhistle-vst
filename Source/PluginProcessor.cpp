/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"
#include "LoopSampler/LoopSamplerVoice.h"
#include "ADSR/ADSRVal.h" //TODO Think of a way to get these includes out of here, feels like they shouldn't be in this class.
#include "LoopSampler/LoopSamplerSound.h"
#include "Panner/PannerComponent.h"
#include "Reverb/ReverbVal.h"
#include "Volume/VolumeComponent.h"


//==============================================================================
NaiteWhistleAudioProcessor::NaiteWhistleAudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  juce::AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", juce::AudioChannelSet::stereo(), true)
                     #endif
                       ), apvt(*this,nullptr,"PARAMETERS",CreateParameters())
#endif
{
	samplerLoader.LoadSamples(sampler,Articulation::Sustain);
	apvt.state.addListener(this);
     for(size_t i{};i < numVoices;i++)
    {
	    sampler.addVoice(new LoopSamplerVoice{});
    }
}

NaiteWhistleAudioProcessor::~NaiteWhistleAudioProcessor()
{
}

//==============================================================================
const juce::String NaiteWhistleAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool NaiteWhistleAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool NaiteWhistleAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool NaiteWhistleAudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double NaiteWhistleAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int NaiteWhistleAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int NaiteWhistleAudioProcessor::getCurrentProgram()
{
    return 0;
}

void NaiteWhistleAudioProcessor::setCurrentProgram (int index)
{
}

const juce::String NaiteWhistleAudioProcessor::getProgramName (int index)
{
    return {};
}

void NaiteWhistleAudioProcessor::changeProgramName (int index, const juce::String& newName)
{
}

//==============================================================================
void NaiteWhistleAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
	sampler.setCurrentPlaybackSampleRate(sampleRate);
	UpdateADSR();
	simpleReverb.setSampleRate(sampleRate);
	UpdateReverb();
}

void NaiteWhistleAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool NaiteWhistleAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    juce::ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    // Some plugin hosts, such as certain GarageBand versions, will only
    // load plugins that support stereo bus layouts.
    if (layouts.getMainOutputChannelSet() != juce::AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != juce::AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void NaiteWhistleAudioProcessor::processBlock (juce::AudioBuffer<float>& buffer, juce::MidiBuffer& midiMessages)
{
    auto totalNumInputChannels  = getTotalNumInputChannels();
    auto totalNumOutputChannels = getTotalNumOutputChannels();

    for (auto i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
    {
        buffer.clear (i, 0, buffer.getNumSamples());
    }
    for(const auto m : midiMessages)
    {
    	//DBG(m.getMessage().getNoteNumber());
    }
	sampler.renderNextBlock(buffer,midiMessages,0,buffer.getNumSamples());

    if(totalNumOutputChannels <= 2)
    {
		simpleReverb.processStereo(buffer.getWritePointer(0),buffer.getWritePointer(1),buffer.getNumSamples());
    }
	
    const float volume = apvt.getRawParameterValue(VolumeComponent::ID)->load();
    const float pannerVol = apvt.getRawParameterValue(PannerComponent::ID)->load();
	
    for(int channel{0}; channel<totalNumOutputChannels; ++channel)
    {
	    float* channelData = buffer.getWritePointer(channel);

        const float finalVolForChannel = volume * (channel == 0 ? 1-pannerVol : 1+pannerVol) * 0.4f;
    	
        for(int sample = 0; sample < buffer.getNumSamples();sample++)
        {
	        channelData[sample] = channelData[sample] * finalVolForChannel;
        }
    }
}

//==============================================================================
bool NaiteWhistleAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

juce::AudioProcessorEditor* NaiteWhistleAudioProcessor::createEditor()
{
    return new NaiteWhistleAudioProcessorEditor (*this);
}

//==============================================================================
void NaiteWhistleAudioProcessor::getStateInformation (juce::MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
}

void NaiteWhistleAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
}

juce::AudioProcessorValueTreeState& NaiteWhistleAudioProcessor::GetAPVT()
{
	return apvt;
}

void NaiteWhistleAudioProcessor::valueTreePropertyChanged(juce::ValueTree& treeWhosePropertyHasChanged,
	const juce::Identifier& property)
{
	UpdateADSR();
	UpdateReverb();
}

void NaiteWhistleAudioProcessor::UpdateADSR()
{
	adsrParams.attack = apvt.getRawParameterValue(attackValue.ID)->load();
	adsrParams.decay = apvt.getRawParameterValue(decayValue.ID)->load();
	adsrParams.sustain = apvt.getRawParameterValue(sustainValue.ID)->load();
	adsrParams.release = apvt.getRawParameterValue(releaseValue.ID)->load();
	
	for(int i{0};i< sampler.getNumSounds();i++)
    {
        if(auto *sound = dynamic_cast<LoopSamplerSound*>(sampler.getSound(i).get()))
        {
	        sound->setEnvelopeParameters(adsrParams);
        }
    }

}

void NaiteWhistleAudioProcessor::UpdateReverb() 
{
	revParams.roomSize = apvt.getRawParameterValue(roomSizeValue.ID)->load();
	revParams.damping = apvt.getRawParameterValue(dampingValue.ID)->load();
	revParams.wetLevel = apvt.getRawParameterValue(wetLevelValue.ID)->load();
	revParams.dryLevel = apvt.getRawParameterValue(dryLevelValue.ID)->load();
    revParams.width = apvt.getRawParameterValue(widthValue.ID)->load();
	revParams.freezeMode = apvt.getRawParameterValue(freezeModeValue.ID)->load();
	
	simpleReverb.setParameters(revParams);
}

juce::AudioProcessorValueTreeState::ParameterLayout NaiteWhistleAudioProcessor::CreateParameters() const
{
	std::vector<std::unique_ptr<juce::RangedAudioParameter>> parametersPtrs;
	parametersPtrs.push_back(std::make_unique<juce::AudioParameterFloat>(attackValue.ID,attackValue.name,attackValue.minValue,attackValue.maxValue,attackValue.defaultValue));
	parametersPtrs.push_back(std::make_unique<juce::AudioParameterFloat>(decayValue.ID,decayValue.name,decayValue.minValue,decayValue.maxValue,decayValue.defaultValue));
	parametersPtrs.push_back(std::make_unique<juce::AudioParameterFloat>(sustainValue.ID,sustainValue.name,sustainValue.minValue,sustainValue.maxValue,sustainValue.defaultValue));
	parametersPtrs.push_back(std::make_unique<juce::AudioParameterFloat>(releaseValue.ID,releaseValue.name,releaseValue.minValue,releaseValue.maxValue,releaseValue.defaultValue));
    parametersPtrs.push_back(std::make_unique<juce::AudioParameterFloat>(VolumeComponent::ID,VolumeComponent::name,VolumeComponent::minValue,VolumeComponent::maxValue,VolumeComponent::defaultValue));
    parametersPtrs.push_back(std::make_unique<juce::AudioParameterFloat>(PannerComponent::ID,PannerComponent::name,PannerComponent::minValue,PannerComponent::maxValue,PannerComponent::defaultValue));

    parametersPtrs.push_back(std::make_unique<juce::AudioParameterFloat>(roomSizeValue.ID,roomSizeValue.name,roomSizeValue.minValue,roomSizeValue.maxValue,roomSizeValue.defaultValue));
    parametersPtrs.push_back(std::make_unique<juce::AudioParameterFloat>(dampingValue.ID,dampingValue.name,dampingValue.minValue,dampingValue.maxValue,dampingValue.defaultValue));
	parametersPtrs.push_back(std::make_unique<juce::AudioParameterFloat>(wetLevelValue.ID,wetLevelValue.name,wetLevelValue.minValue,wetLevelValue.maxValue,wetLevelValue.defaultValue));
	parametersPtrs.push_back(std::make_unique<juce::AudioParameterFloat>(dryLevelValue.ID,dryLevelValue.name,dryLevelValue.minValue,dryLevelValue.maxValue,dryLevelValue.defaultValue));
	parametersPtrs.push_back(std::make_unique<juce::AudioParameterFloat>(widthValue.ID,widthValue.name,widthValue.minValue,widthValue.maxValue,widthValue.defaultValue));
	parametersPtrs.push_back(std::make_unique<juce::AudioParameterFloat>(freezeModeValue.ID,freezeModeValue.name,freezeModeValue.minValue,freezeModeValue.maxValue,freezeModeValue.defaultValue));
	
	
	return {parametersPtrs.begin(),parametersPtrs.end()};
}

//==============================================================================
// This creates new instances of the plugin..
juce::AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new NaiteWhistleAudioProcessor();
}
