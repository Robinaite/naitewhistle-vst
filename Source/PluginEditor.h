/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"
#include "ADSR/ADSRComponent.h"
#include "Panner/PannerComponent.h"
#include "Reverb/ReverbComponent.h"
#include "Volume/VolumeComponent.h"

//==============================================================================
/**
*/
class NaiteWhistleAudioProcessorEditor  : public juce::AudioProcessorEditor
{
public:
    NaiteWhistleAudioProcessorEditor (NaiteWhistleAudioProcessor&);
    ~NaiteWhistleAudioProcessorEditor() override;

    //==============================================================================
    void paint (juce::Graphics&) override;
    void resized() override;

private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    NaiteWhistleAudioProcessor& audioProcessor;
	ADSRComponent adsrComp;
	VolumeComponent volumeComponent;
	PannerComponent pannerComponent;
	ReverbComponent reverbComponent;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (NaiteWhistleAudioProcessorEditor)
};
