# Naite Tin Whistle VST

![Whistle](https://i.imgur.com/EhtXn8V.png)

Naite Tin Whistle VST is a small vst I made using samples I've recorded myself of the Tin Whistle.

**Download here:** https://gitlab.com/Robinaite/naitewhistle-vst/-/raw/master/Download/NaiteTinWhistle-V2.zip?inline=false

## Demo Recording

![Legend Of Zelda: Lost Wood](LoZ-LostWoods_NaiteTinWhistle_Example.wav)
![Legend Of Zelda: Lost Wood - Tin Whistle Only](LoZ-LostWoods-tinwhistleonly.wav)

## Features

- VST3
- ADSR
- Volume
- Panner
- Reverb
- Simple to use
- Notes between D5 and C#7
- Works on Windows and Linux (Doesn't work on Mac)

## How to open the project

1. Download Juce: https://juce.com/
2. Open Projuicer.exe
3. In Projuicer open NaiteWhistle.jucer file
4. Open in your preferred IDE from there.

